# 配置文件

```yaml
server:
  port: 8098
  servlet:
    context-path: /fastpreview
spring:
  resources:
    static-locations: classpath:static/,file:tempfiles/analysis/
preview:
  privateNetworkFileMap: []
  imageExt: .jpg;.png;.gif
  vedioExt: .mp4;.asf;.avi;.dat;.f4v;.flv;.mkv;.mov;.mpg;.rmvb;.ts;.vob;.webm;.wmv;.vob
  txtExt: .txt
  officeConvertMethod: 2
office:
  home: H:/LibreOffice/
  ports: 2001,2002
  timeout: 5m
```

<table style="width:650px">
    <tr>
        <td style="width:150px">参数名称</td>
         <td style="width:500px">备注</td>
    </tr>
     <tr>
        <td style="width:150px">privateNetworkFileMap</td>
         <td style="width:500px">文件ip映射，每项格式为，外网地址|内网地址</td>
    </tr>
      <tr>
        <td style="width:150px">imageExt</td>
         <td style="width:500px">图片支持格式</td>
    </tr>
      <tr>
        <td style="width:150px">vedioExt</td>
         <td style="width:500px">视频支持格式</td>
    </tr>
    <tr>
        <td style="width:150px">txtExt</td>
         <td style="width:500px">文本支持格式</td>
    </tr>  
     <tr>
        <td style="width:150px">officeConvertMethod</td>
         <td style="width:500px">office文件预览方式 1：aspose（默认） 2：libreoffice</td>
    </tr>  
</table>

<table style="width:650px">
    <tr>
        <td style="width:150px">参数名称</td>
         <td style="width:500px">备注</td>
    </tr>
     <tr>
        <td style="width:150px">home</td>
         <td style="width:500px">libreoffice安装目录</td>
    </tr>
      <tr>
        <td style="width:150px">ports</td>
         <td style="width:500px">libreoffic开启端口</td>
    </tr>
      <tr>
        <td style="width:150px">timeout</td>
         <td style="width:500px">libreoffice转换超时时间</td>
    </tr>
</table>

# 预览方案

| 文件类型      | 预览方案                                                     |
| ------------- | ------------------------------------------------------------ |
| word          | aspsoe-word转换图片/PDF预览（版本21.1）/ 安装LibreOffice7.5.8 |
| ppt           | aspose-slides转换图片预览（版本20.4）/ 安装LibreOffice7.5.8  |
| excel         | aspose-cell转换html预览（版本20.4）/ 安装LibreOffice7.5.8    |
| pdf           | pdfbox转换图片预览（版本2.0.15） 整个pdf.js直接预览（版本1.6.0） |
| png，jpg，gif | 整合viewer.js预览（版本1.5.0）                               |
| mp4           | 整合vedio.js预览（js版本7.10.2） 整合ffmpeg转换视频格式（版本3.3.1） |
| txt           | 读取文件内容预览                                             |

# 使用方法

直接运行项目，输入预览地址

http://localhost:8098/fastpreview/index.html?file=（文件地址）

文件地址支持文件访问路径与流输出

word文件和pdf文件支持以pdf格式预览，在预览地址后加入参数&model=pdf

# 预览效果

## **word**

![](https://code2roc-blog.oss-cn-beijing.aliyuncs.com/blogimage/SpringBoot%E5%AE%9E%E7%8E%B0%E6%96%87%E4%BB%B6%E5%9C%A8%E7%BA%BF%E9%A2%84%E8%A7%88/word-preview.png)

## **excel-aspose**

![](https://code2roc-blog.oss-cn-beijing.aliyuncs.com/blogimage/SpringBoot%E5%AE%9E%E7%8E%B0%E6%96%87%E4%BB%B6%E5%9C%A8%E7%BA%BF%E9%A2%84%E8%A7%88/excel-preview.png)

## **excel-libreoffce**

![](https://code2roc-blog.oss-cn-beijing.aliyuncs.com/blogimage/SpringBoot%E5%AE%9E%E7%8E%B0%E6%96%87%E4%BB%B6%E5%9C%A8%E7%BA%BF%E9%A2%84%E8%A7%88/excel-preview-libreoffice.png)

## **ppt**

![](https://code2roc-blog.oss-cn-beijing.aliyuncs.com/blogimage/SpringBoot%E5%AE%9E%E7%8E%B0%E6%96%87%E4%BB%B6%E5%9C%A8%E7%BA%BF%E9%A2%84%E8%A7%88/ppt-preview.png)

## **pdf**

![](https://code2roc-blog.oss-cn-beijing.aliyuncs.com/blogimage/SpringBoot%E5%AE%9E%E7%8E%B0%E6%96%87%E4%BB%B6%E5%9C%A8%E7%BA%BF%E9%A2%84%E8%A7%88/pdf-preview.png)

## **image**

![](https://code2roc-blog.oss-cn-beijing.aliyuncs.com/blogimage/SpringBoot%E5%AE%9E%E7%8E%B0%E6%96%87%E4%BB%B6%E5%9C%A8%E7%BA%BF%E9%A2%84%E8%A7%88/image-preview.png)

## **vedio**

![](https://code2roc-blog.oss-cn-beijing.aliyuncs.com/blogimage/SpringBoot%E5%AE%9E%E7%8E%B0%E6%96%87%E4%BB%B6%E5%9C%A8%E7%BA%BF%E9%A2%84%E8%A7%88/vedio=preview.png)

## **txt**

![](https://code2roc-blog.oss-cn-beijing.aliyuncs.com/blogimage/SpringBoot%E5%AE%9E%E7%8E%B0%E6%96%87%E4%BB%B6%E5%9C%A8%E7%BA%BF%E9%A2%84%E8%A7%88/txt-preview.png)

# 字体问题

## windows

拷贝字体文件到C:\Windows\Fonts目录，重启系统

## linux

字体要拷贝文件到/usr/share/fonts目录

建立索引信息，更新字体缓存

```
mkfontscale
mkfontdir
fc-cache
```

如果安装失败可以考虑修改权限

```
Chmod 755 *
```

安装完成，可以用命令查看系统中的中文字体是否安装成功

```
fc-list :lang=zh
```

