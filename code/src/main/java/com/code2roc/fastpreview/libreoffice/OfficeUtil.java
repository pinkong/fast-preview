package com.code2roc.fastpreview.libreoffice;

import org.jodconverter.local.LocalConverter;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class OfficeUtil {

    public static void convertFile(File inputFile, File outputFile) {
        try {
            Map<String, Object> customProperties = new HashMap<>();
            LocalConverter.Builder builder = LocalConverter.builder().storeProperties(customProperties);
            builder.build().convert(inputFile).to(outputFile).execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
