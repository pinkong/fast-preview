package com.code2roc.fastpreview.handle;

import com.aspose.words.Document;
import com.code2roc.fastpreview.config.PreviewFileTypeConfig;
import com.code2roc.fastpreview.model.FileConvertInfo;
import com.code2roc.fastpreview.util.FileAnalysisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;

@FileConvert(".doc|.docx")
@Service
public class DocFileConvert implements IFileConvert {
    @Autowired
    private FileAnalysisUtil fileAnalysisUtil;
    @Autowired
    private PreviewFileTypeConfig previewFileTypeConfig;

    @Override
    public FileConvertInfo convertFile(FileConvertInfo fileConvertInfo) {
        try {
            fileConvertInfo.setConvertType("doc");
            Document doc = new Document(fileConvertInfo.getFilePath());
            fileConvertInfo.setPageNum(doc.getPageCount());
            File file = null;
            if(fileConvertInfo.getModel().equals("pdf")){
                file = new File(fileConvertInfo.getFileDirPath() + "convert.pdf");
            }else{
                file = new File(fileConvertInfo.getFileDirPath() + "split_1.jpeg");
            }
            if(file.exists()){
                fileConvertInfo.setHaveConvert(true);
                return fileConvertInfo;
            }
            if(previewFileTypeConfig.getOfficeConvertMethod().equals("1")){
                fileAnalysisUtil.convertFileToPrevie(fileConvertInfo);
            }else{
                fileAnalysisUtil.convertOfficeFileByLibreOffice(fileConvertInfo);
            }
        } catch (Exception e) {
            fileConvertInfo.setErrorInfo("转换doc文档发生错误");
            e.printStackTrace();
        }
        return fileConvertInfo;
    }
}
