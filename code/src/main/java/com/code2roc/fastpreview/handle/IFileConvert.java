package com.code2roc.fastpreview.handle;

import com.code2roc.fastpreview.model.FileConvertInfo;

public interface IFileConvert {
    FileConvertInfo convertFile(FileConvertInfo fileConvertInfo);
}
