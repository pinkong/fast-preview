package com.code2roc.fastpreview.handle;

import com.code2roc.fastpreview.model.FileConvertInfo;
import com.code2roc.fastpreview.util.FileAnalysisUtil;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;

@FileConvert(".pdf")
@Service
public class PDFFileConvert  implements IFileConvert{
    @Autowired
    private FileAnalysisUtil fileAnalysisUtil;

    @Override
    public FileConvertInfo convertFile(FileConvertInfo fileConvertInfo) {
        try {
            fileConvertInfo.setConvertType("pdf");
            PDDocument pdf = PDDocument.load(new File((fileConvertInfo.getFilePath())));
            fileConvertInfo.setPageNum(pdf.getNumberOfPages());
            File file = null;
            if(fileConvertInfo.getModel().equals("pdf")){
                file = new File(fileConvertInfo.getFileDirPath() + "convert.pdf");
            }else{
                file = new File(fileConvertInfo.getFileDirPath() + "split_1.jpeg");
            }
            if(file.exists()){
                fileConvertInfo.setHaveConvert(true);
                return fileConvertInfo;
            }
            pdf.close();
            fileAnalysisUtil.convertFileToPrevie(fileConvertInfo);
        } catch (Exception e) {
            fileConvertInfo.setErrorInfo("转换pdf文档发生错误");
            e.printStackTrace();
        }
        return fileConvertInfo;
    }
}
