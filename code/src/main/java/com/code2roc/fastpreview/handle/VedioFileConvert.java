package com.code2roc.fastpreview.handle;

import com.code2roc.fastpreview.model.FileConvertInfo;
import com.code2roc.fastpreview.util.FileAnalysisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;

@FileConvert(".asf|.avi|.avi|.dat|.f4v|.flv|.mkv|.mov|.mpg|.rmvb|.ts|.vob|.webm|.wmv|.vob")
@Service
public class VedioFileConvert  implements IFileConvert {
    @Autowired
    private FileAnalysisUtil fileAnalysisUtil;

    @Override
    public FileConvertInfo convertFile(FileConvertInfo fileConvertInfo) {
        try {
            fileConvertInfo.setConvertType("vedioconvert");
            File file = new File(fileConvertInfo.getFileDirPath() + "convert.mp4");
            if(file.exists()){
                fileConvertInfo.setHaveConvert(true);
                return fileConvertInfo;
            }
            fileAnalysisUtil.convertFileToPrevie(fileConvertInfo);
        } catch (Exception e) {
            fileConvertInfo.setErrorInfo("视频转码发生错误");
            e.printStackTrace();
        }
        return fileConvertInfo;
    }
}
