package com.code2roc.fastpreview.handle;

import com.aspose.cells.Workbook;
import com.code2roc.fastpreview.config.PreviewFileTypeConfig;
import com.code2roc.fastpreview.model.FileConvertInfo;
import com.code2roc.fastpreview.util.FileAnalysisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;

@FileConvert(".xls|.xlsx")
@Service
public class ExcelFileConvert implements IFileConvert {
    @Autowired
    private FileAnalysisUtil fileAnalysisUtil;
    @Autowired
    private PreviewFileTypeConfig previewFileTypeConfig;

    @Override
    public FileConvertInfo convertFile(FileConvertInfo fileConvertInfo) {
        try {
            fileConvertInfo.setConvertType("excel");
            Workbook wb = new Workbook(fileConvertInfo.getFilePath());
            fileConvertInfo.setPageNum(1);
            File file = new File(fileConvertInfo.getFileDirPath() + "convert.html");
            if (file.exists()) {
                fileConvertInfo.setHaveConvert(true);
                return fileConvertInfo;
            }
            if (previewFileTypeConfig.getOfficeConvertMethod().equals("1")) {
                fileAnalysisUtil.convertFileToPrevie(fileConvertInfo);
            } else {
                fileAnalysisUtil.convertOfficeFileByLibreOffice(fileConvertInfo);
            }
        } catch (Exception e) {
            fileConvertInfo.setErrorInfo("转换excel文档发生错误");
            e.printStackTrace();
        }
        return fileConvertInfo;
    }

}
