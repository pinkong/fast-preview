package com.code2roc.fastpreview.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@ConfigurationProperties(prefix="preview")
public class PreviewFileTypeConfig {
    //图片预览文件类型
    private String imageExt;
    //视频预览文件类型
    private String vedioExt;
    //文本预览文件类型
    private String txtExt;
    private List<String> privateNetworkFileMap;
    //office文件转换方式 1:aspose 2:liboffice
    private String officeConvertMethod;

    public PreviewFileTypeConfig(){
        imageExt = ".jpg;.png;.gif";
        vedioExt = ".mp4";
        txtExt = ".txt";
        privateNetworkFileMap = new ArrayList<>();
        officeConvertMethod = "1";
    }

    public String getImageExt() {
        return imageExt;
    }

    public void setImageExt(String imageExt) {
        this.imageExt = imageExt;
    }

    public String getVedioExt() {
        return vedioExt;
    }

    public void setVedioExt(String vedioExt) {
        this.vedioExt = vedioExt;
    }

    public String getTxtExt() {
        return txtExt;
    }

    public void setTxtExt(String txtExt) {
        this.txtExt = txtExt;
    }

    public List<String> getPrivateNetworkFileMap() {
        return privateNetworkFileMap;
    }

    public void setPrivateNetworkFileMap(List<String> privateNetworkFileMap) {
        this.privateNetworkFileMap = privateNetworkFileMap;
    }

    public String getOfficeConvertMethod() {
        return officeConvertMethod;
    }

    public void setOfficeConvertMethod(String officeConvertMethod) {
        this.officeConvertMethod = officeConvertMethod;
    }
}
