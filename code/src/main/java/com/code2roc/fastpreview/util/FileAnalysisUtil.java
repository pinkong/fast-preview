package com.code2roc.fastpreview.util;

import com.aspose.cells.HtmlSaveOptions;
import com.aspose.cells.Workbook;
import com.aspose.cells.Worksheet;
import com.aspose.slides.ISlide;
import com.aspose.slides.Presentation;
import com.aspose.words.Document;
import com.aspose.words.FontSettings;
import com.aspose.words.SaveFormat;
import com.code2roc.fastpreview.config.PreviewFileTypeConfig;
import com.code2roc.fastpreview.handle.FileConvert;
import com.code2roc.fastpreview.handle.IFileConvert;
import com.code2roc.fastpreview.libreoffice.OfficeUtil;
import com.code2roc.fastpreview.model.FileConvertInfo;
import com.sun.xml.internal.messaging.saaj.util.ByteOutputStream;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.jodconverter.core.DocumentConverter;
import org.jodconverter.core.document.DefaultDocumentFormatRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ws.schild.jave.process.ProcessWrapper;
import ws.schild.jave.process.ffmpeg.DefaultFFMPEGLocator;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.*;

@Component
public class FileAnalysisUtil {
    private Map<String, IFileConvert> handleConvertRulesMap = new HashMap<>();
    @Autowired
    private ApplicationContext applicationContext;
    @Autowired
    private PreviewFileTypeConfig previewFileTypeConfig;

    private static Map<String, String> types;
    private static List<String> extensions = new ArrayList<String>();
    public final static Map<String, String> FILE_TYPE_MAP = new HashMap<String, String>();

    public void initFileConvertRules() {
        try {
            Map<String, IFileConvert> res = applicationContext.getBeansOfType(IFileConvert.class);
            for (Map.Entry en : res.entrySet()) {
                IFileConvert service = (IFileConvert) en.getValue();
                FileConvert fileConvert = (FileConvert) en.getValue().getClass().getAnnotation(FileConvert.class);
                String[] convertTypelist = fileConvert.value().split("\\|");
                for (String fileExt : convertTypelist) {
                    handleConvertRulesMap.put(fileExt, service);
                }
            }
            System.out.println("系统初始化文件转换策略成功");
        } catch (Exception e) {
            e.printStackTrace();
        }

        getAllFileType();
        initContentType();
        initExtentsion();
    }

    public String readFile(String fileUrl) throws Exception {
        URLConnection conn = HttpUtil.getConnection(fileUrl);
        String fileGuid = "";
        String realUrl = conn.getURL().toString();
        String redirectUrl = conn.getHeaderField("Location");
        if (!StringUtils.isEmpty(redirectUrl)) {
            realUrl = redirectUrl;
        }
        //处理文件地址，如果有网址映射则进行替换
        if (previewFileTypeConfig.getPrivateNetworkFileMap() != null && previewFileTypeConfig.getPrivateNetworkFileMap().size() > 0) {
            for (String regular : previewFileTypeConfig.getPrivateNetworkFileMap()) {
                System.out.println(regular);
                String[] info = regular.split("\\|");
                String reciveFileAddress = info[0];
                String replaceFileAddress = info[1];
                realUrl = realUrl.replace(reciveFileAddress, replaceFileAddress);
            }
        }
        String fileExt = getFileNameExtFromUrl(realUrl);
        System.out.println("redirectUrl:" + redirectUrl);
        System.out.println("fileExt:" + fileExt);
        if (previewFileTypeConfig.getImageExt().contains(fileExt) || fileExt.equals(".mp4")) {
            fileGuid = "onlineresource|" + fileUrl;
            return fileGuid;
        }
        if (handleConvertRulesMap.containsKey(fileExt) || previewFileTypeConfig.getTxtExt().contains(fileExt)) {
            fileGuid = EncryptUtil.encryptByMD5(fileUrl);
            String analysisPath = "tempfiles/";
            File saveDir = new File(analysisPath);
            if (!saveDir.exists()) {
                saveDir.mkdir();
            }
            analysisPath = "tempfiles/analysis/";
            saveDir = new File(analysisPath);
            if (!saveDir.exists()) {
                saveDir.mkdir();
            }
            analysisPath = analysisPath + fileGuid + "/";
            saveDir = new File(analysisPath);
            if (!saveDir.exists()) {
                saveDir.mkdir();
            }
            File file = new File(saveDir + File.separator + fileGuid + fileExt);
            if (!file.exists()) {
                file.createNewFile();
                conn = HttpUtil.getConnection(realUrl);
                InputStream inputStream = conn.getInputStream();
                byte[] getData = FileUtil.convertStreamToByte(inputStream);
                FileOutputStream fos = new FileOutputStream(file);
                fos.write(getData);
                if (fos != null) {
                    fos.close();
                }

                if (inputStream != null) {
                    inputStream.close();
                }
            }
        }
        return fileGuid;
    }

    public String getFileNameExtFromUrl(String fileUrl) throws Exception {
        URLConnection conn = HttpUtil.getConnection(fileUrl);
        String contentType = conn.getContentType();
        String disposition = conn.getHeaderField("Content-disposition");
        conn.connect();
        InputStream inputStream = conn.getInputStream();
        ByteOutputStream byteOutputStream = new ByteOutputStream();
        byte[] bytes = new byte[1024];
        int len = 0;
        byteOutputStream.write(bytes, 0, 50);
        byte[] byteArray = byteOutputStream.toByteArray();
        inputStream.close();
        byteOutputStream.close();
        String fileType = getFileType(fileUrl, disposition, contentType, byteArray);
        return fileType;
    }

    ///region 读取文件后缀

    public static String getFileType(String url, String disposition, String contentType, byte[] byteArray) {
        String ext = null;
        ext = getTypeByExtenssion(url);
        {
            if (ext != null)
                return ext;
        }
        ext = getTypeByDisposition(disposition);
        {
            if (ext != null)
                return ext;
        }
        ext = getTypeByContentType(contentType);
        {
            if (ext != null)
                return ext;
        }
        ext = getFileTypeByStream(byteArray);
        {
            if (ext != null)
                return ext;
        }

        return ".html";
    }

    private static String getFileTypeByStream(byte[] b) {
        if (ArrayUtils.isNotEmpty(b)) {
            b = ArrayUtils.subarray(b, 0, 50);
            String filetypeHex = String.valueOf(getFileHexString(b));
            Iterator<Map.Entry<String, String>> entryiterator = FILE_TYPE_MAP.entrySet().iterator();
            while (entryiterator.hasNext()) {
                Map.Entry<String, String> entry = entryiterator.next();
                String fileTypeHexValue = entry.getValue();
                if (filetypeHex.toUpperCase().startsWith(fileTypeHexValue)) {
                    return entry.getKey();
                }
            }
        }
        return null;
    }

    private static String getFileHexString(byte[] b) {
        StringBuilder stringBuilder = new StringBuilder();
        if (b == null || b.length <= 0) {
            return null;
        }
        for (int i = 0; i < b.length; i++) {
            int v = b[i] & 0xFF;
            String hv = Integer.toHexString(v);
            if (hv.length() < 2) {
                stringBuilder.append(0);
            }
            stringBuilder.append(hv);
        }
        return stringBuilder.toString();
    }

    public static String getTypeByExtenssion(String linkUrl) {
        if (linkUrl == null)
            return null;
        linkUrl = linkUrl.toLowerCase();
        String filterUrl = linkUrl.split("\\?")[0];
        for (String ext : extensions) {
            if (filterUrl.endsWith(ext)) {
                return ext;
            }
        }
        return null;
    }

    private static String getTypeByDisposition(String disposition) {
        String ext = null;
        if (!StringUtils.isEmpty(disposition)) {
            disposition = StringUtils.replace(disposition, "\"", "");
            String[] strs = disposition.split(";");
            for (String string : strs) {
                if (string.toLowerCase().indexOf("filename=") >= 0) {
                    ext = StringUtils.substring(string, string.lastIndexOf("."));
                    break;
                }
            }
        }
        return ext;
    }

    private static String getTypeByContentType(String contentType) {
        if (types.containsKey(contentType))
            return types.get(contentType);
        return null;
    }

    // 通过文件头判断会出现重复的情况
    private static void getAllFileType() {
        FILE_TYPE_MAP.put(".pdf", "255044462D312E"); // Adobe Acrobat (pdf)
        FILE_TYPE_MAP.put(".doc", "D0CF11E0"); // MS Word
        FILE_TYPE_MAP.put(".xls", "D0CF11E0"); // MS Excel 注意：word 和 excel的文件头一样
        FILE_TYPE_MAP.put(".jpg", "FFD8FF"); // JPEG (jpg)
        FILE_TYPE_MAP.put(".png", "89504E47"); // PNG (png)
        FILE_TYPE_MAP.put(".gif", "47494638"); // GIF (gif)
        FILE_TYPE_MAP.put(".tif", "49492A00"); // TIFF (tif)
        FILE_TYPE_MAP.put(".bmp", "424D"); // Windows Bitmap (bmp)
        FILE_TYPE_MAP.put(".dwg", "41433130"); // CAD (dwg)
        FILE_TYPE_MAP.put(".html", "68746D6C3E"); // HTML (html)
        FILE_TYPE_MAP.put(".rtf", "7B5C727466"); // Rich Text Format (rtf)
        FILE_TYPE_MAP.put(".xml", "3C3F786D6C");
        FILE_TYPE_MAP.put(".zip", "504B0304"); // docx的文件头与zip的一样
        FILE_TYPE_MAP.put(".rar", "52617221");
        FILE_TYPE_MAP.put(".psd", "38425053"); // Photoshop (psd)
        FILE_TYPE_MAP.put(".eml", "44656C69766572792D646174653A"); // Email
        FILE_TYPE_MAP.put(".dbx", "CFAD12FEC5FD746F"); // Outlook Express (dbx)
        FILE_TYPE_MAP.put(".pst", "2142444E"); // Outlook (pst)
        FILE_TYPE_MAP.put(".mdb", "5374616E64617264204A"); // MS Access (mdb)
        FILE_TYPE_MAP.put(".wpd", "FF575043"); // WordPerfect (wpd)
        FILE_TYPE_MAP.put(".eps", "252150532D41646F6265");
        FILE_TYPE_MAP.put(".ps", "252150532D41646F6265");
        FILE_TYPE_MAP.put(".qdf", "AC9EBD8F"); // Quicken (qdf)
        FILE_TYPE_MAP.put(".pwl", "E3828596"); // Windows Password (pwl)
        FILE_TYPE_MAP.put(".wav", "57415645"); // Wave (wav)
        FILE_TYPE_MAP.put(".avi", "41564920");
        FILE_TYPE_MAP.put(".ram", "2E7261FD"); // Real Audio (ram)
        FILE_TYPE_MAP.put(".rm", "2E524D46"); // Real Media (rm)
        FILE_TYPE_MAP.put(".mpg", "000001BA"); //
        FILE_TYPE_MAP.put(".mov", "6D6F6F76"); // Quicktime (mov)
        FILE_TYPE_MAP.put(".asf", "3026B2758E66CF11"); // Windows Media (asf)
        FILE_TYPE_MAP.put(".mid", "4D546864"); // MIDI (mid)
    }

    // 对应的http contenttype
    private static void initContentType() {
        types = new HashMap<String, String>();
        types.put("application/pdf", ".pdf");
        types.put("application/msword", ".doc");
        types.put("text/plain", ".txt");
        types.put("application/javascript", ".js");
        types.put("application/x-xls", ".xls");
        types.put("application/-excel", ".xls");
        types.put("text/html", ".html");
        types.put("application/x-rtf", ".rtf");
        types.put("application/x-ppt", ".ppt");
        types.put("image/jpeg", ".jpg");
        types.put("application/vnd.openxmlformats-officedocument.wordprocessingml.template", ".docx");
        types.put("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", ".xlsx");
        types.put("application/vnd.openxmlformats-officedocument.presentationml.presentation", ".pptx");
        types.put("message/rfc822", ".eml");
        types.put("application/xml", ".xml");
    }

    //自定义需要匹配链接的文件后缀，不满足的可以自行添加
    private static void initExtentsion() {
        extensions.add(".pdf");
        extensions.add(".doc");
        extensions.add(".txt");
        extensions.add(".xls");
        extensions.add(".html");
        extensions.add(".rtf");
        extensions.add(".mht");
        extensions.add(".rar");
        extensions.add(".ppt");
        extensions.add(".jpg");
        extensions.add(".docx");
        extensions.add(".xlsx");
        extensions.add(".pptx");
        extensions.add(".eml");
        extensions.add(".zip");
        extensions.add(".docm");
        extensions.add(".xlsm");
        extensions.add(".xlsb");
        extensions.add(".dox");
        extensions.add(".csv");

        extensions.add(".pdf");
        extensions.add(".doc");
        extensions.add(".xls");
        extensions.add(".jpg");
        extensions.add(".png");
        extensions.add(".gif");
        extensions.add(".tif");
        extensions.add(".bmp");
        extensions.add(".dwg");
        extensions.add(".html");
        extensions.add(".rtf");
        extensions.add(".xml");
        extensions.add(".zip");
        extensions.add(".rar");
        extensions.add(".psd");
        extensions.add(".eml");
        extensions.add(".dbx");
        extensions.add(".pst");
        extensions.add(".mdb");
        extensions.add(".wpd");
        extensions.add(".eps");
        extensions.add(".ps");
        extensions.add(".qdf");
        extensions.add(".pwl");
        extensions.add(".wav");
        extensions.add(".avi");
        extensions.add(".ram");
        extensions.add(".rm");
        extensions.add(".mpg");
        extensions.add(".mov");
        extensions.add(".asf");
        extensions.add(".mid");
        extensions.add(".mp4");
    }
    ///endregion

    public String getFileNameExtFromPath(String filePath) throws IOException {
        String[] spliteArray = filePath.split("\\.");
        String fileExt = "." + spliteArray[spliteArray.length - 1];
        return fileExt.toLowerCase();
    }

    public FileConvertInfo convertFile(String fileGuid, String model) throws Exception {
        if (fileGuid.startsWith("onlineresource")) {
            String fileUrl = fileGuid.replace("onlineresource|", "");
            URLConnection conn = HttpUtil.getConnection(fileUrl);
            String realUrl = conn.getURL().toString();
            String fileExt = getFileNameExtFromUrl(realUrl);
            FileConvertInfo fileConvertInfo = new FileConvertInfo();
            if (previewFileTypeConfig.getImageExt().contains(fileExt)) {
                fileConvertInfo.setConvertType("image");
            } else if (previewFileTypeConfig.getVedioExt().contains(fileExt)) {
                fileConvertInfo.setConvertType("vedio");
            }
            return fileConvertInfo;
        } else {
            String tempFileDicPath = "tempfiles/analysis/" + fileGuid + "/";
            File fileDic = new File(tempFileDicPath);
            File[] flist = fileDic.listFiles();
            String filePath = "";
            String fileExt = "";
            String fileName = "";
            for (File f : flist) {
                if (f.isFile() && f.getName().startsWith(fileGuid)) {
                    filePath = f.getAbsolutePath();
                    fileExt = getFileNameExtFromPath(filePath);
                    fileName = f.getName();
                    break;
                }
            }
            FileConvertInfo fileConvertInfo = new FileConvertInfo();
            fileConvertInfo.setFileGuid(fileGuid);
            fileConvertInfo.setFileName(fileName);
            fileConvertInfo.setFileDirPath(tempFileDicPath);
            fileConvertInfo.setFilePath(filePath);
            fileConvertInfo.setFileExt(fileExt);
            fileConvertInfo.setModel(model);
            if (previewFileTypeConfig.getTxtExt().contains(fileExt)) {
                fileConvertInfo.setConvertType("txt");
                return fileConvertInfo;
            } else {
                return handleConvertRulesMap.get(fileExt).convertFile(fileConvertInfo);
            }
        }
    }

    public String getFileContent(String fileGuid) {
        String fileContent = "";
        try {
            String tempFileDicPath = "tempfiles/analysis/" + fileGuid + "/";
            File fileDic = new File(tempFileDicPath);
            File[] flist = fileDic.listFiles();
            if (flist.length > 0) {
                File file = flist[0];
                String encoding = "UTF-8";

                Long filelength = file.length();
                byte[] filecontent = new byte[filelength.intValue()];
                FileInputStream in = new FileInputStream(file);
                in.read(filecontent);
                fileContent = new String(filecontent, encoding);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fileContent;
    }

    @Async
    public void convertFileToPrevie(FileConvertInfo fileConvertInfo) throws Exception {
        try {
            switch (fileConvertInfo.getConvertType()) {
                case "doc":
                    initFontSetting();
                    Document doc = new Document(fileConvertInfo.getFilePath());
                    if (fileConvertInfo.getModel().equals("pdf")) {
                        String convertTemplateFilePath = fileConvertInfo.getFileDirPath() + "converttemp.pdf";
                        String convertFilePath = fileConvertInfo.getFileDirPath() + "convert.pdf";
                        doc.save(convertTemplateFilePath, SaveFormat.PDF);
                        File oldFile = new File(convertTemplateFilePath);
                        File newFile = new File(convertFilePath);
                        oldFile.renameTo(newFile);
                    } else {
                        for (int i = 0; i < doc.getPageCount(); i++) {
                            Document extractedPage = doc.extractPages(i, 1);
                            String convertTemplateFilePath = fileConvertInfo.getFileDirPath() + "tempsplit_" + (i + 1) + ".jpeg";
                            extractedPage.save(convertTemplateFilePath, SaveFormat.JPEG);
                            String convertFilePath = fileConvertInfo.getFileDirPath() + "split_" + (i + 1) + ".jpeg";
                            File oldFile = new File(convertTemplateFilePath);
                            File newFile = new File(convertFilePath);
                            oldFile.renameTo(newFile);
                        }
                    }
                    break;
                case "ppt":
                    Presentation ppt = new Presentation(fileConvertInfo.getFilePath());
                    for (int i = 0; i < ppt.getSlides().size(); i++) {
                        ISlide slide = ppt.getSlides().get_Item(i);
                        int height = (int) (ppt.getSlideSize().getSize().getHeight() - 150);
                        int width = (int) (ppt.getSlideSize().getSize().getWidth() - 150);
                        BufferedImage image = slide.getThumbnail(new java.awt.Dimension(width, height));
                        //每一页输出一张图片
                        String convertTemplateFilePath = fileConvertInfo.getFileDirPath() + "tempsplit_" + (i + 1) + ".jpeg";
                        File outImage = new File(convertTemplateFilePath);
                        ImageIO.write(image, "jpeg", outImage);
                        String convertFilePath = fileConvertInfo.getFileDirPath() + "split_" + (i + 1) + ".jpeg";
                        File oldFile = new File(convertTemplateFilePath);
                        File newFile = new File(convertFilePath);
                        oldFile.renameTo(newFile);
                    }
                    break;
                case "excel":
                    ///region 转化图片(废弃)
                    //Workbook wb = new Workbook(fileConvertInfo.getFilePath());
                    //ImageOrPrintOptions imgOptions = new ImageOrPrintOptions();
                    //imgOptions.setImageFormat(ImageFormat.getJpeg());
                    //for (int i = 0; i < wb.getWorksheets().getCount(); i++) {
                    // Worksheet sheet = wb.getWorksheets().get(i);
                    //SheetRender sr = new SheetRender(sheet, imgOptions);
                    //sr.toImage(i, fileConvertInfo.getFileDirPath() + "split_" + (i + 1) + ".jpeg");
                    //}
                    //endregion
                    Workbook wb = new Workbook(fileConvertInfo.getFilePath());
                    HtmlSaveOptions opts = new HtmlSaveOptions();
                    opts.setExportWorksheetCSSSeparately(true);
                    opts.setExportSimilarBorderStyle(true);
                    Worksheet ws = wb.getWorksheets().get(0);
                    wb.save(fileConvertInfo.getFileDirPath() + "convert.html", opts);
                    break;
                case "pdf":
                    if (fileConvertInfo.getModel().equals("pdf")) {
                        String convertFilePath = fileConvertInfo.getFileDirPath() + "convert.pdf";
                        FileUtil.copyFile(fileConvertInfo.getFilePath(), convertFilePath);
                    } else {
                        PDDocument pdf = PDDocument.load(new File((fileConvertInfo.getFilePath())));
                        int pageCount = pdf.getNumberOfPages();
                        PDFRenderer renderer = new PDFRenderer(pdf);
                        for (int i = 0; i < pageCount; i++) {
                            BufferedImage image = renderer.renderImage(i, 1.25f); // 第二个参数越大生成图片分辨率越高，转换时间也就越长
                            String convertTemplateFilePath = fileConvertInfo.getFileDirPath() + "tempsplit_" + (i + 1) + ".jpeg";
                            ImageIO.write(image, "JPEG", new File(convertTemplateFilePath));
                            String convertFilePath = fileConvertInfo.getFileDirPath() + "split_" + (i + 1) + ".jpeg";
                            File oldFile = new File(convertTemplateFilePath);
                            File newFile = new File(convertFilePath);
                            oldFile.renameTo(newFile);
                        }
                        pdf.close();
                    }
                    break;
                case "vedioconvert":
                    ProcessWrapper ffmpeg = new DefaultFFMPEGLocator().createExecutor();
                    ffmpeg.addArgument("-i");
                    ffmpeg.addArgument(fileConvertInfo.getFilePath());
                    ffmpeg.addArgument("-c:v");
                    ffmpeg.addArgument("libx264");
                    ffmpeg.addArgument("-crf");
                    ffmpeg.addArgument("19");
                    ffmpeg.addArgument("-strict");
                    ffmpeg.addArgument("experimental");
                    ffmpeg.addArgument(fileConvertInfo.getFileDirPath() + "convert.mp4");
                    ffmpeg.execute();
                    try (BufferedReader br = new BufferedReader(new InputStreamReader(ffmpeg.getErrorStream()))) {
                        blockFfmpeg(br);
                    }
                    File file = new File(fileConvertInfo.getFileDirPath() + "finish.txt");
                    file.createNewFile();
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Async
    public void convertOfficeFileByLibreOffice(FileConvertInfo fileConvertInfo) throws Exception {
        try {
            if (fileConvertInfo.getConvertType().equals("doc")) {
                String convertFilePath = fileConvertInfo.getFileDirPath() + "convert.pdf";
                File newFile = new File(convertFilePath);
                if (!newFile.exists()) {
                    String convertTemplateFilePath = fileConvertInfo.getFileDirPath() + "converttemp.pdf";
                    File sourceFile = new File(fileConvertInfo.getFilePath());
                    File targetFile = new File(convertTemplateFilePath);
                    if (!targetFile.exists()) {
                        targetFile.createNewFile();
                    }
                    OfficeUtil.convertFile(sourceFile,targetFile);
                    File oldFile = new File(convertTemplateFilePath);
                    oldFile.renameTo(newFile);
                }
                if (!fileConvertInfo.getModel().equals("pdf")) {
                    PDDocument pdf = PDDocument.load(new File((convertFilePath)));
                    int pageCount = pdf.getNumberOfPages();
                    PDFRenderer renderer = new PDFRenderer(pdf);
                    for (int i = 0; i < pageCount; i++) {
                        BufferedImage image = renderer.renderImage(i, 1.25f); // 第二个参数越大生成图片分辨率越高，转换时间也就越长
                        String convertTemplateFilePath = fileConvertInfo.getFileDirPath() + "tempsplit_" + (i + 1) + ".jpeg";
                        ImageIO.write(image, "JPEG", new File(convertTemplateFilePath));
                        String convertJPGFilePath = fileConvertInfo.getFileDirPath() + "split_" + (i + 1) + ".jpeg";
                        File oldJPFFile = new File(convertTemplateFilePath);
                        File newJPFFile = new File(convertJPGFilePath);
                        oldJPFFile.renameTo(newJPFFile);
                    }
                    pdf.close();
                }
            } else if (fileConvertInfo.getConvertType().equals("ppt")) {
                String convertFilePath = fileConvertInfo.getFileDirPath() + "convert.pdf";
                File newFile = new File(convertFilePath);
                if (!newFile.exists()) {
                    String convertTemplateFilePath = fileConvertInfo.getFileDirPath() + "converttemp.pdf";
                    File sourceFile = new File(fileConvertInfo.getFilePath());
                    File targetFile = new File(convertTemplateFilePath);
                    if (!targetFile.exists()) {
                        targetFile.createNewFile();
                    }
                    OfficeUtil.convertFile(sourceFile,targetFile);
                    File oldFile = new File(convertTemplateFilePath);
                    oldFile.renameTo(newFile);
                }
                PDDocument pdf = PDDocument.load(new File((convertFilePath)));
                int pageCount = pdf.getNumberOfPages();
                PDFRenderer renderer = new PDFRenderer(pdf);
                for (int i = 0; i < pageCount; i++) {
                    BufferedImage image = renderer.renderImage(i, 1.25f); // 第二个参数越大生成图片分辨率越高，转换时间也就越长
                    String convertTemplateFilePath = fileConvertInfo.getFileDirPath() + "tempsplit_" + (i + 1) + ".jpeg";
                    ImageIO.write(image, "JPEG", new File(convertTemplateFilePath));
                    String convertJPGFilePath = fileConvertInfo.getFileDirPath() + "split_" + (i + 1) + ".jpeg";
                    File oldJPFFile = new File(convertTemplateFilePath);
                    File newJPFFile = new File(convertJPGFilePath);
                    oldJPFFile.renameTo(newJPFFile);
                }
                pdf.close();
            } else if (fileConvertInfo.getConvertType().equals("excel")) {
                String sourcFilePath = fileConvertInfo.getFilePath();
                String converFilePath = fileConvertInfo.getFileDirPath() + "convert.html";
                File sourceFile = new File(sourcFilePath);
                File targetFile = new File(converFilePath);
                if (!targetFile.exists()) {
                    targetFile.createNewFile();
                }
                OfficeUtil.convertFile(sourceFile,targetFile);
                //二次处理excel
                StringBuilder sb = new StringBuilder();
                try (InputStream inputStream = new FileInputStream(converFilePath);
                     BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
                    String line;
                    while (null != (line = reader.readLine())) {
                        if (line.contains("charset=gb2312")) {
                            line = line.replace("charset=gb2312", "charset=utf-8");
                        }
                        sb.append(line);
                    }
                    // 添加sheet控制头
                    sb.append("<script src=\"../js/jquery-3.6.1.min.js\"></script>");
                    sb.append("<script src=\"../js/excel.js\"></script>");
                    sb.append("<link rel=\"stylesheet\" href=\"../css/excel.css\">");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                // 重新写入文件
                try (FileOutputStream fos = new FileOutputStream(converFilePath);
                     BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(fos, StandardCharsets.UTF_8))) {
                    writer.write(sb.toString());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void initFontSetting() {
        String os = System.getProperty("os.name"); //获取当前java运行
        if (os != null && os.toLowerCase().startsWith("windows")) {
        } else if (os != null && os.toLowerCase().startsWith("linux")) { //当前环境：linux系统
            //apose在linux服务器上生成pdf，内容乱码问题，解决代码：将windows的字体上传到linux，取linux上的字体列表
            FontSettings fontSettings = FontSettings.getDefaultInstance();
            fontSettings.setFontsFolder("/usr/share/fonts" + File.separator, true);
        }
    }

    private static void blockFfmpeg(BufferedReader br) throws IOException {
        String line;
        // 该方法阻塞线程，直至合成成功
        while ((line = br.readLine()) != null) {
            doNothing(line);
        }
    }

    private static void doNothing(String line) {
        System.out.println(line);
    }
}
