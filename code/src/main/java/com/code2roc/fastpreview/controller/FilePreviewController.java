package com.code2roc.fastpreview.controller;


import com.code2roc.fastpreview.config.PreviewFileTypeConfig;
import com.code2roc.fastpreview.model.FileConvertInfo;
import com.code2roc.fastpreview.model.Result;
import com.code2roc.fastpreview.util.ConvertOp;
import com.code2roc.fastpreview.util.FileAnalysisUtil;
import com.code2roc.fastpreview.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.util.Map;

@RestController
@RequestMapping("/common/filepreview")
public class FilePreviewController {
    @Autowired
    private FileAnalysisUtil fileAnalysisUtil;
    @Autowired
    private PreviewFileTypeConfig previewFileTypeConfig;

    @PostMapping("/checkFile")
    @ResponseBody
    public Object checkFile(@RequestBody Map<String, Object> params) {
        String file = ConvertOp.convert2String(params.get("file"));
       //file="https://zcmanage.obs.cidc-rp-12.joint.cmecloud.cn/202210/202210280842041b4b3097-0ea8-4d33-bd84-37e98cd34943/zhongwen.doc?AccessKeyId=VRPJC7IQOS5OIHOUI87U&Expires=1667517818&Signature=DifQbh7WnDsGpEh%2Fk26vCy1WrK4%3D";
        //处理文件地址，如果有网址映射则进行替换
        if (previewFileTypeConfig.getPrivateNetworkFileMap() != null && previewFileTypeConfig.getPrivateNetworkFileMap().size() > 0) {
            for (String regular : previewFileTypeConfig.getPrivateNetworkFileMap()) {
                System.out.println(regular);
                String[] info = regular.split("\\|");
                String reciveFileAddress = info[0];
                String replaceFileAddress = info[1];
                file = file.replace(reciveFileAddress, replaceFileAddress);
            }
        }
        Result result = Result.okResult();
        try {
            if (StringUtil.isEmpty(file)) {
                return Result.errorResult().setMsg("未指定预览文件地址");
            }
            String fileGuid = fileAnalysisUtil.readFile(file);
            if (StringUtil.isEmpty(fileGuid)) {
                return Result.errorResult().setMsg("不支持文件的预览类型");
            }
            result.add("fileGuid", fileGuid);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(file);
            result = Result.errorResult().setMsg("文件地址错误");
        }
        return result;
    }


    @PostMapping("/getFileContent")
    @ResponseBody
    public Object getFileContent(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        String fileGuid = ConvertOp.convert2String(params.get("fileGuid"));
        String fileContent = fileAnalysisUtil.getFileContent(fileGuid);
        result.add("fileContent", fileContent);
        return result;
    }

    @PostMapping("/convertFile")
    @ResponseBody
    public Object convertFile(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        String fileGuid = ConvertOp.convert2String(params.get("fileGuid"));
        String model = ConvertOp.convert2String(params.get("model"));
        try {
            FileConvertInfo fileConvertInfo = fileAnalysisUtil.convertFile(fileGuid, model);
            if (!StringUtil.isEmpty(fileConvertInfo.getErrorInfo())) {
                result = Result.errorResult().setMsg("文件转换失败");
            }
            result.add("obj", fileConvertInfo);
        } catch (Exception e) {
            result = Result.errorResult().setMsg("文件转换失败");
            e.printStackTrace();
        }
        return result;
    }

    @PostMapping("/checkConvertImageFile")
    @ResponseBody
    public Object checkConvertFile(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        String fileGuid = ConvertOp.convert2String(params.get("fileGuid"));
        int pageNum = ConvertOp.convert2Int(params.get("pageNum"));
        int maxFileIndex = 0;
        for (int i = 1; i <= pageNum; i++) {
            String filePath = "tempfiles/analysis/" + fileGuid + "/" + "split_" + i + ".jpeg";
            File file = new File(filePath);
            if (file.exists() && file.length() > 0) {
                maxFileIndex = i;
            } else {
                break;
            }
        }
        result.add("maxFileIndex", maxFileIndex);
        return result;
    }

    @PostMapping("/checkConvertExcelHtmlFile")
    @ResponseBody
    public Object checkConvertExcelHtmlFile(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        String fileGuid = ConvertOp.convert2String(params.get("fileGuid"));
        String filePath = "tempfiles/analysis/" + fileGuid + "/" + "convert.html";
        boolean checkConvert = false;
        File file = new File(filePath);
        if (file.exists() && file.length() > 0) {
            checkConvert = true;
        }
        result.add("checkConvert", checkConvert);
        return result;
    }

    @PostMapping("/checkConvertPDFFile")
    @ResponseBody
    public Object checkConvertPDFFile(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        String fileGuid = ConvertOp.convert2String(params.get("fileGuid"));
        String filePath = "tempfiles/analysis/" + fileGuid + "/" + "convert.pdf";
        boolean checkConvert = false;
        File file = new File(filePath);
        if (file.exists() && file.length() > 0) {
            checkConvert = true;
        }
        result.add("checkConvert", checkConvert);
        return result;
    }

    @PostMapping("/checkConvertVedioFile")
    @ResponseBody
    public Object checkConvertVedioFile(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        String fileGuid = ConvertOp.convert2String(params.get("fileGuid"));
        String filePath = "tempfiles/analysis/" + fileGuid + "/" + "finish.txt";
        boolean checkConvert = false;
        File file = new File(filePath);
        if (file.exists()) {
            checkConvert = true;
        }
        result.add("checkConvert", checkConvert);
        return result;
    }
}
