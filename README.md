<p align="center">
	<img alt="logo" src="https://code2roc-image.oss-cn-beijing.aliyuncs.com/fastpreview.png" width="150" height="150">
</p>
<h1 align="center" style="margin: 30px 0 30px; font-weight: bold;">FastPreview</h1>
<h4 align="center">一个轻量级在线预览解决方案，部署简单，无外部依赖！</h4>
<hr style="height:1px;border:none;border-top:1px solid #ccc;" />

## 前言：

- [在线文档：https://code2roc.gitee.io/fast-preview/](https://code2roc.gitee.io/fast-preview/)

- 欢迎大家在各种服务器环境下测试，一起完善项目，有问题请提issues

- 开源不易，点个 star 鼓励一下吧！


## FastPreview 介绍

**FastPreview** 是一个在线文件预览服务，无外部依赖，开箱即用

支持office预览（word/excel/ppt）及pdf预览

支持图片预览（jpg，png，gif等）及视频预览（mp4，asf，avi等）

支持文件地址/文件流两种指定方式

你只需要进行以下步骤就可以快速使用

- 下载 [最新发行版](https://gitee.com/code2roc/fast-preview/releases/)
- 执行java -jar fastpreview-[last-version].jar 命令运行
- 访问http://localhost:8098/fastpreview/index.html?file=（文件地址）

